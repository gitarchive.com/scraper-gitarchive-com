# scraper.gitarchive.com

## Introduction

Scraper (or crawler) is an API that asks for resources to be fetched and save their raw data within our usercontent instances. 

It's pretty sure that the intelligence of how frequently a typical URL should be fetched will be within the scraper. Anyhow, the workflow will evolve on this, as we will start by scraping every resources once a day only.

## Initial workflow (v0.1)

This is a quick and dirty scraper workflow. It works enough for a limited number of resources to be fetched once a day (but it has no intelligence built-in):

1. Request from API.gitarchive.com about 100 resources to be fetched ;
2. Make a simple `http` or `https` GET requests to the distant-resource ;
3. Save the raw data (both headers and body) into usercontent ;
4. Trigger a commit on api.gitarchive.com.
5. Repeat as long as there are resources to be fetched. If there's none, keep asking every 5 minutes for new resources.

