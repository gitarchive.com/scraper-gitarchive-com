// const Dataset = require('./etc/dataset')
const lib		= require("gitarchive_lib");
const Sentry 		= require('@sentry/node');
const APIRequest	= lib.request;
const iFetch		= lib.Ifetch;
const logger		= lib.logger;
const APIError		= lib.error;
const config		= require("./config")

/**
 * Initialize Sentry
 * more: https://docs.sentry.io/platforms/node/express/
 */

Sentry.init({ dsn: config.services.sentry });

let dataset = [], i = 0;

// Get resources to be fetched
function	scraper() {
	APIRequest.scraper(config.services.api).getResources()
	.then(ResourcesList => { dataset = ResourcesList.data; })
	.then(scraping)
	.catch(e => {
		logger.error(APIError.badImplementation("Error when trying to launch scraper service") + e);
	})
}

// Scrap resources
async function scraping () {

	const ResourceItem = dataset[i];

	if (!ResourceItem)
		{ return logger.info('Scraping ended (total: ' + i + ').'); }

	// Intelligent fetch of distant-resources (one at a time)
	await new iFetch(ResourceItem)
	.then(saving)
	.catch(error => {
		if (error.fetchResponse && error.fetchResponse.statusCode && error.fetchResponse.statusCode) {
			return APIRequest.resource(config.services.api, ResourceItem)
			.createFetch(error.fetchRequest, error.fetchResponse)
		}
		else { logger.error("Can't fetch remote resource: " + error); }
	});

	// Continue with next scraping
	i++;
	return scraping();
};

// Post raw data within API's usercontent
function saving ([resource, request, response]) {

	const rsrc = function() { return APIRequest.resource(config.services.api, resource); };

	// Save raw headers
	return rsrc().setRawHeaders(response.headers)

	// then save raw body
	.then(() => { return rsrc().setRawBody(response.body); })

	// then save request information (fetch)
	.then(() => { return rsrc().createFetch(request, response); })

	// then create a commit
	.then((FetchItem) => { return rsrc().createCommit(FetchItem, request, response); })

	// handle errors (if any /^_^/ )
	.catch(err => {
		throw APIError.badImplementation("Can't save commit to usercontent" + err);
	});
};

// Start scraping resources
setInterval(() => {
	logger.info('Start scraping');
	scraper();
}, 5 * 60 * 1000);
